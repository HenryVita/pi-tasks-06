#pragma once

#include "group.h"
#include "student.h"
#include <time.h>
using namespace std;

class Dekanat
{
	private:
		
		Group** gr;			//������ �� �����;
		Student** st;		//������ ���������;
		int numGr;			//���������� �����;
		int numSt;			//���������� ���������;

	public:

		Dekanat();

		Dekanat(const Dekanat &d);

		~Dekanat();

		//������ �� ����� �����;
		void addNewGroupInFile();

		//������ �� ����� ���������;
		void addNewStudentInFile();

		//���������� ������ �������� �� id;
		int addMarksID(int id, int mark);

		//���������� ��������� ������ ���� ��������� ��������� 
		//(� ��������� ���������� ������);
		void addRandMarks(int n);

		//��������� ������� ������ �� ���� �������;
		double getAvMark();

		//���������� ��������� �� ���� ������� � �������� ����������� �������;
		int howStudWithMinMark(int n);

		//������� �������� �� id � ������ ������;
		int transferStud(int id, string title);

	private:

		void removeStud(Student* stud);

	public:
		//���������� ������������;
		void allBadStudDELETE();

		//���������� ������ � ������;
		void saveBase();

		//����� �������� (�� ������ ������� �� ����������);
		void setAllRandHead();

		//����� ������ �� �������;
		void getGroupList();
		void getInfomation();
};
