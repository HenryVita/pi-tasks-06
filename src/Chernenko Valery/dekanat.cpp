#include<time.h>
#include <iostream>
#include<fstream>
#include<string>

#include"dekanat.h"
#include "student.h"
#include "group.h"

using namespace std;
	
	//������������;
	Dekanat::Dekanat()
	{
		gr = nullptr;
		st = nullptr;
		numGr = 0;
		numSt = 0;
	}

	Dekanat::Dekanat(const Dekanat& d)
	{
		numGr = d.numGr;
		numSt = d.numSt;
		gr = new Group*[numGr];
		st = new Student*[numSt];
		for (int i = 0; i < numGr; i++)
			gr[i] = d.gr[i];
		for (int i = 0; i < numSt; i++)
			st[i] = d.st[i];
	}
	//����������;
	Dekanat::~Dekanat()
	{
		for (int i = 0; i < numGr; i++)
			delete gr[i];
		delete[] gr;
		for (int i = 0; i < numSt; i++)
			delete st[i];
		delete[] st;
	}

	//������ �� ����� �����;
	void Dekanat::addNewGroupInFile()
	{
		string str;
		int count = 0;
		ifstream file("groups.txt");
		if (!file)
		{
			cout << "���� � �������� �� ��� ������!" << endl;
			return;
		}
		while (!file.eof())
		{
			getline(file, str);
			if (str == "")
				break;
			count++;
		}
		numGr = count;
		gr = new Group*[numGr];
		file.seekg(0, ios::beg);
		int i = 0;
		while (!file.eof())
		{
			getline(file, str);
			gr[i++] = new Group(str);
		}
		file.close();
	}

	//������ �� ����� ���������;
	void Dekanat::addNewStudentInFile()
	{
		if (!numGr)
		{
			cout << "�� ������� ������!" << endl;
			return;
		}
		int id;
		string f,i,o;
		string title;
		ifstream file("students.txt");
		if (!file)
		{
			cout << "���� �� ���������� �� ������!!!" << endl;
			return;
		}
		int count = 0;
		while(!file.eof())
		{
			string buf;
			getline(file, buf);
			count++;
		}
		numSt = count;
		st = new Student*[numSt];
		file.seekg(0, ios::beg);
		int k = 0;
		int j = 0;
		while (!file.eof())
		{
			file >> id >> f >> i >> o >> title; 
			st[k] = new Student(id, f+" "+i+" "+o);
			for(j = 0; j < numGr && gr[j] -> getTitle() != title ; j++);
			if(j == numGr)
			{
				cout << "������� � id = "<< id << " �� ������� � ������ �� ��������� ���������!!!" << endl;
				bool trans = false;
				while (!trans)
				{
					string buf;
					cout << "=======================================================================" << endl;
					this->getGroupList();
					cout << "�������� ������, ���� ������ ��������� ��������: ";
					cin >> buf;
					int l;
					for (l = 0; l < numGr && gr[l]->getTitle() != buf; l++);
					if (l == numGr)
						cout << "��������� ������ �� �������!" << endl;
					else
					{
						gr[l]->addStud(st[k]);
						trans = true;
					}
				}
			}
			else 
			{
				gr[j] -> addStud(st[k]);
			}
			k++;
		}
	}

	//���������� ������ �������� �� id;
	int Dekanat::addMarksID(int id, int mark)
	{
		int i;
		for (i = 0; i < numSt && st[i]->getID() != id; i++);
		if (i == numSt)
			return 0;
		else
			st[i]->addMark(mark);
		return i;
	}
	
	//���������� ��������� ������ ���� ��������� ��������� 
	//(� ��������� ������������ ���������� ������);
	void Dekanat::addRandMarks(int n)
	{
		int luck;
		#ifndef _STUDRAND_
		#define _STUDRAND_
		srand((unsigned int)time);
		#endif
		for (int i = 0; i < numSt; i++)
		{
			for (int j = 0; j < n; j++)
			{
				luck = rand() % 101; //����� ���� ����� ����������� ���������� ������;
				if (luck > 60)
					st[i]->addMark(5);
				else if (luck > 20)
					st[i]->addMark(4);
				else if (luck > 5)
					st[i]->addMark(3);
				else
					st[i]->addMark(2);
			}
		}
	}
	
	//��������� ������� ������ �� ���� �������;
	double Dekanat::getAvMark()
	{
		if (!numGr)
			return 0;
		double sum = 0;
		double corNum = 0;
		for (int i = 0; i < numGr; i++)
		{
			double tmp = gr[i]->getAvMark();
			if (tmp)
			{
				sum += tmp;
				corNum++;
			}
		}
		if (!corNum)
			return 0;
		else return sum / corNum;
	}
	
	//���������� ��������� � �������� ����������� �������;
	int Dekanat::howStudWithMinMark(int n)
	{
		int count = 0;
		for(int i = 0; i < numSt; i++)
			if(st[i] -> getMinMarks() == n)
				count ++;
		return count;
	}

	//���������� ��������;
	void Dekanat::removeStud(Student* stud)
	{
		int i;
		for (i = 0; i < numSt; i++)
			if (st[i] == stud)
				break;
		if (i == numSt)
		{
			cout << "������� �� ������! ��� �� ��� ������ �����?" << endl;
			return;
		}
		for (int j = i; j < numSt - 1; j++)
			st[j] = st[j + 1];
		numSt--;
		Student** tmp = new Student*[numSt];
		for (int j = 0; j < numSt; j++)
			tmp[j] = st[j];
		delete[] st;
		st = new Student*[numSt];
		for (int j = 0; j < numSt; j++)
			st[j] = tmp[j];
		delete[] tmp;
	}

	//������� �������� �� id � ������ ������;
	int Dekanat::transferStud(int id, string title)
	{
		int i;
		int j;
		for (i = 0; i < numGr && gr[i]->getTitle() != title; i++);
		cout << "������� ��������� �������� � id = " << id << " � ������ " << title << " :" << endl;
		if (i == numGr)
		{
			cout << "������ �� �������!" << endl;
			return 0;
		}
		for (j = 0; j < numSt && st[j]->getID() != id; j++)
		if (j == numSt)
		{
			cout << "������� � ��������� id �� ������!" << endl;
			return 0;
		}
		if (st[j]->getGroup() == gr[i])
		{
			cout << "C������ ��� � ������ ������ ������!" << endl;
			return 1;
		}
		cout << "������� � ������: " << st[j]->getGroup()->getTitle() << endl;
		st[j]->getGroup()->removeStud(id);
		gr[i]->addStud(st[j]);
		cout << "C������ ��������� : id = " << id << " ����� ������ - " << (gr[i]->getTitle()) << endl;
		return 0;
	}

	//���������� ������������;
	void Dekanat::allBadStudDELETE()
	{
		for (int i = 0; i < numSt; i++)
			if (st[i]->howMarks(2) >= 3)
			{
				cout << "������� � id = " << st[i]->getID() << " ������ �����! ��������))" << endl;
				st[i]->getGroup()->removeStud(st[i]->getID());
				removeStud(st[i]);
				i--;
			}
	}

	//���������� ������ � ������;
	void Dekanat::saveBase()
	{
		ofstream fout;
		fout.open("Dekanat.txt");
		for (int i = 0; i < numGr; i++)
		{
			fout << *gr[i];
			fout << "������� ������ �� ������: " << gr[i]->getAvMark() << endl;
			fout << endl;
		}
		cout << "���������� ���� ��������� � �����:" << endl;
		fout.close();
	}

	//����� �������� (�� ������ �������);
	void Dekanat::setAllRandHead()
	{
		for (int i = 0; i < numGr; i++)
			gr[i]->setHead(gr[i]->getRandStud());
	}

	//����� ������ �� �������;
	void Dekanat::getGroupList()
	{
		cout << "������ �����:" << endl;
		for (int i = 0; i < numGr; i++)
		{
			string str = gr[i]->getTitle();
			cout << str << endl;
		}
	}
	void Dekanat::getInfomation()
	{
		for (int i = 0; i < numSt; i++)
		{
			cout << "������: " << st[i]->getGroup()->getTitle() << *st[i] << endl;
		}
	}
